import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'

Vue.config.productionTip = false

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    session: null
  },
  mutations: {
    setNewSession (state, sessionId) {
      document.cookie = "JSESSIONID=" + sessionId;
      state.session = sessionId;
    }
  }
});

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
